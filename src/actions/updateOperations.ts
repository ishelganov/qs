import { UPDATE_OPERATIONS } from '../constants';

type Options = {
    operation: 'add' | 'remove' | 'change';
    item: {
        id: string;
        parentId?: string;
        value?: string;
        childIds?: string[];
        removed: boolean;
    };
};

/**
 * Save all changes in local cache to send then them all to backend
 * @param options
 */
const updateOperations = (options: Options) => ({
    type: UPDATE_OPERATIONS,
    payload: {
        data: options,
    },
});

export default updateOperations;
