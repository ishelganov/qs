import * as React from 'react';
import { shallowEqual, useSelector } from 'react-redux';

import { rootState } from '../../reducers';

import AddControl from '../../components/AddControl';
import RemoveControl from '../RemoveControl';
import ChangeControl from '../../components/ChangeControl';
import ApplyControl from '../ApplyControl';
import ResetControlContainer from '../ResetControl';

type Props = {
    className: string;
};

const CacheTreeControlsContainer: React.FC<Props> = ({ className }) => {
    const selectedNode = useSelector((state: rootState) => state.ui.activeCacheNode);
    const items = useSelector((state: rootState) => state.cache.items, shallowEqual);

    const disabled = React.useMemo(() => {
        return !selectedNode || (items[selectedNode] && items[selectedNode].removed);
    }, [selectedNode, items]);

    return (
        <div className={className}>
            <AddControl disabled={disabled} />
            <RemoveControl disabled={disabled} />
            <ChangeControl disabled={disabled} />
            <ApplyControl />
            <ResetControlContainer />
        </div>
    );
};

export default CacheTreeControlsContainer;
