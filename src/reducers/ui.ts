import {
    RESET_APP,
    SET_SELECTED_CACHE_NODE,
    SET_SELECTED_DB_NODE,
    ADD_NOTIFICATION,
    REMOVE_NOTIFICATION,
} from '../constants';

import { selectCacheNode, selectDbNode } from '../actions/selectNode';
import resetApp from '../actions/resetApp';
import addNotification from '../actions/addNotification';
import removeNotification from '../actions/removeNotification';

type UiState = {
    activeCacheNode: string | null;
    activeDbNode: string | null;
    notification: string;
};

const defaultState = {
    activeCacheNode: null,
    activeDbNode: null,
    notification: '',
};

type Actions =
    | ReturnType<typeof selectCacheNode>
    | ReturnType<typeof selectDbNode>
    | ReturnType<typeof resetApp>
    | ReturnType<typeof addNotification>
    | ReturnType<typeof removeNotification>;

const ui = (state: UiState = defaultState, action: Actions) => {
    switch (action.type) {
        case SET_SELECTED_CACHE_NODE: {
            return {
                ...state,
                activeCacheNode: action.payload.data.id,
            };
        }

        case SET_SELECTED_DB_NODE: {
            return {
                ...state,
                activeDbNode: action.payload.data.id,
            };
        }

        case ADD_NOTIFICATION: {
            const { text } = action.payload.data;

            return {
                ...state,
                notification: text,
            };
        }

        case REMOVE_NOTIFICATION: {
            return {
                ...state,
                notification: '',
            };
        }

        case RESET_APP: {
            return defaultState;
        }

        default:
            return state;
    }
};

export default ui;
