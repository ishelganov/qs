import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';

import addNode from 'src/actions/addNode';
import updateOperations from '../../actions/updateOperations';

import { rootState } from '../../reducers';

import Modal from '../../components/Modal';

type Props = {
    closePortal: () => void;
};

const NewNodeModalContainer: React.FC<Props> = ({ closePortal }) => {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState('');
    const selectedNode = useSelector((state: rootState) => state.ui.activeCacheNode);

    const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
    };

    const onClickSave = () => {
        if (selectedNode) {
            const newNode = {
                id: uuidv4(),
                value,
                childIds: [],
                parentId: selectedNode,
                removed: false,
            };
            dispatch(addNode(newNode));
            dispatch(
                updateOperations({
                    operation: 'add',
                    item: {
                        parentId: newNode.parentId,
                        id: newNode.id,
                        value: newNode.value,
                        childIds: newNode.childIds,
                        removed: newNode.removed,
                    },
                }),
            );
        }

        closePortal();
    };

    return (
        <Modal
            title={'Enter the value'}
            value={value}
            onClickSave={onClickSave}
            onChangeInput={onChangeInput}
        />
    );
};

export default NewNodeModalContainer;
