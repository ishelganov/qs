import * as React from 'react';
import cn from 'classnames';

import './style.scss';

type Props = {
    className?: string;
};

const Paper: React.FC<Props> = ({ className, children }) => {
    return <div className={cn('Paper', className)}>{children}</div>;
};

export default Paper;
