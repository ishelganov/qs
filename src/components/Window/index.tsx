import * as React from 'react';
import './style.scss';

type Props = {};

const Window: React.FC<Props> = ({children}) => {
    return (
        <div className={'Window'}>
            {children}
        </div>
    );
};

export default Window;