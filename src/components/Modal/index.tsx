import * as React from 'react';

import Button from '../Button';

import './style.scss';

type Props = {
    title: string;
    value: string;
    onClickSave: () => void;
    onChangeInput: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const Modal: React.FC<Props> = ({ title, value, onClickSave, onChangeInput }) => {
    return (
        <div className={'Modal'}>
            <div className={'Modal-Content'}>
                <div className={'Modal-Title'}>{title}</div>
                <input className={'Modal-Input'} type='text' onChange={onChangeInput} value={value} />
                <Button disabled={!value} onClick={onClickSave}>
                    Ok
                </Button>
            </div>
        </div>
    );
};

export default Modal;
