import * as React from 'react';
import ReactLoader from 'react-loader-spinner';

import './style.scss';

type Props = {};

const Loader: React.FC<Props> = () => {
    return (
        <div className={'Loader'}>
            <ReactLoader width={40} height={40} color={'#445e8f'} type={'BallTriangle'}/>
        </div>
    );
};

export default Loader;
