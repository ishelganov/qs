import { REMOVE_NODE } from '../constants';

type Options = {
    id: string;
};

/**
 * Remove node node
 * @param options
 */
export const removeNode = (options: Options) => ({
    type: REMOVE_NODE,
    payload: {
        data: options,
    },
});

export default removeNode;
