import * as React from 'react';
import { PortalWithState } from 'react-portal';

import Button from '../Button';
import NewNodeModalContainer from '../../containers/NewNodeModal';

type Props = {
    disabled: boolean;
};

const AddControl: React.FC<Props> = ({ disabled }) => {
    return (
        <PortalWithState closeOnEsc={true} closeOnOutsideClick={true}>
            {({ openPortal, closePortal, portal }) => (
                <>
                    <Button disabled={disabled} onClick={openPortal}>
                        +
                    </Button>
                    {portal(<NewNodeModalContainer closePortal={closePortal} />)}
                </>
            )}
        </PortalWithState>
    );
};

export default React.memo(AddControl);
