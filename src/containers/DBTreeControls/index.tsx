import * as React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

import { getNode } from '../../actions/getNode';

import { rootState } from '../../reducers';

import Button from '../../components/Button';
import updateCacheTree from '../../actions/updateCacheTree';
import addNotification from '../../actions/addNotification';
import removeNotification from '../../actions/removeNotification';
import updateCacheItems from "../../actions/updateCacheItems";

type Props = {
    className: string;
};

const DBTreeControls: React.FC<Props> = ({ className }) => {
    const dispatch: ThunkDispatch<rootState, unknown, AnyAction> = useDispatch();
    const items = useSelector((state: rootState) => state.db.items, shallowEqual);
    const selectedNode = useSelector((state: rootState) => state.ui.activeDbNode);

    /**
     * Prohibited to click if nothing has been selected && prohibited to load removed items
     */
    const isBtnDisabled = React.useMemo(
        () => !selectedNode || (items[selectedNode] && items[selectedNode].removed),
        [selectedNode, items],
    );

    /**
     * Get node element from database
     */
    const getNodeFromDB = React.useCallback(() => {
        selectedNode &&
            dispatch(getNode(items[selectedNode]))
                .then(() => dispatch(updateCacheTree()))
                .then(() => dispatch(updateCacheItems()))
                .catch(() => {
                    dispatch(addNotification('element exist'));
                    setTimeout(() => {
                        dispatch(removeNotification());
                    }, 3500);
                })

    }, [dispatch, selectedNode, items]);

    return (
        <Button className={className} disabled={isBtnDisabled} onClick={getNodeFromDB}>
            Download
        </Button>
    );
};

export default DBTreeControls;
