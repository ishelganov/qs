import * as React from 'react';

import './style.scss';

import Node from '../Node';

type Props = {
    // list: {
    //     [id: string]: {
    //         id: string;
    //         value: string;
    //     };
    // };
    list: string[];
    type: 'cache' | 'db'
};

const NodeList: React.FC<Props> = ({ list, type }) => {
    return <div></div>;
    // return (
    //     <ul className={'NodeList'}>
    //         {list.map( itemId => {
    //             return (
    //                 <Node
    //                     id={itemId}
    //                     key={itemId}
    //                     type={type}
    //                 />
    //             );
    //         })}
    //     </ul>
    // );
};

export default NodeList;
