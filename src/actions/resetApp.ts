import { RESET_APP } from '../constants';

const resetApp = () => ({
    type: RESET_APP,
});

export default resetApp;
