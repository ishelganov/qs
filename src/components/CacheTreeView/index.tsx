import * as React from 'react';


import CacheTreeControlsContainer from '../../containers/CacheTreeControls';
import Tree from '../../components/Tree';
import Window from '../../components/Window';

import './style.scss';
import Loader from "../Loader";

type Props = {
    isFetching: boolean;
    tree: string[];
};

const CacheTreeView: React.FC<Props> = ({ isFetching, tree }) => {
    return (
        <div className={'CacheTreeView'}>
            <Window>
                {isFetching && <Loader />}
                <Tree nodes={tree} type={'cache'} />
            </Window>
            <CacheTreeControlsContainer className={'CacheTreeView-Controls'} />
        </div>
    );
};

export default CacheTreeView;
