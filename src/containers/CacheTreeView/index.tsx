import * as React from 'react';
import { useSelector } from 'react-redux';

import { rootState } from '../../reducers';

import CacheTreeView from '../../components/CacheTreeView';

type Props = {};

const CacheTreeViewContainer: React.FC<Props> = () => {
    const tree = useSelector((state: rootState) => state.cache.tree);
    const isFetching = useSelector((state: rootState) => state.cache.isFetching);

    return <CacheTreeView tree={tree} isFetching={isFetching} />;
};

export default CacheTreeViewContainer;
