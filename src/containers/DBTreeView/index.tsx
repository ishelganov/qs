import * as React from 'react';
import { useSelector } from 'react-redux';

import { rootState } from '../../reducers';

import DBTreeView from '../../components/DBTreeView';

type Props = {};

const DBTreeViewContainer: React.FC<Props> = () => {
    const tree = useSelector((state: rootState) => state.db.tree);

    return <DBTreeView tree={tree} />;
};

export default DBTreeViewContainer;
