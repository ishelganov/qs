export const SET_SELECTED_CACHE_NODE = 'SET_SELECTED_CACHE_NODE' as const;
export const SET_SELECTED_DB_NODE = 'SET_SELECTED_DB_NODE' as const;

export const GET_NODE_REQUEST = 'GET_NODE_REQUEST' as const;
export const GET_NODE_SUCCESS = 'GET_NODE_SUCCESS' as const;

export const UPDATE_CACHE_TREE = 'UPDATE_CACHE_TREE' as const;
export const UPDATE_CACHE_ITEMS = 'UPDATE_CACHE_ITEMS' as const;

export const ADD_NODE = 'ADD_NODE' as const;
export const REMOVE_NODE = 'REMOVE_NODE' as const;
export const CHANGE_NODE = 'CHANGE_NODE' as const;

export const RESET_CACHE = 'RESET_CACHE' as const;
export const RESET_APP = 'RESET_APP' as const;
export const RESET_OPERATIONS = 'RESET_OPERATIONS' as const;

export const UPDATE_OPERATIONS = 'UPDATE_OPERATIONS' as const;

export const UPDATE_DB_TREE_SUCCESS = 'UPDATE_DB_TREE_SUCCESS' as const;
export const UPDATE_DB_TREE_REQUEST = 'UPDATE_DB_TREE_REQUEST' as const;

export const ADD_NOTIFICATION = 'ADD_NOTIFICATION' as const;
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION' as const;

export const SYNC_CACHE = 'SYNC_CACHE' as const;