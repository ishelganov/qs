import * as React from 'react';
import Window from '../Window';
import Tree from '../Tree';
import DBTreeControls from '../../containers/DBTreeControls';

import './style.scss';
type Props = {
    tree: string[];
};

const DBTreeView: React.FC<Props> = ({ tree }) => {
    return (
        <div className={'DBTreeView'}>
            <DBTreeControls  className={'DBTreeView-Button'}/>
            <Window>
                <Tree nodes={tree} type={'db'} />
            </Window>
        </div>
    );
};

export default DBTreeView;
