import * as React from 'react';
import Button from '../../components/Button';
import { useDispatch } from 'react-redux';
import resetApp from '../../actions/resetApp';

type Props = {};

const ResetControlContainer: React.FC<Props> = () => {
    const dispatch = useDispatch();

    const resetOnClick = () => {
        dispatch(resetApp());
    };

    return <Button onClick={resetOnClick}>Reset</Button>;
};

export default ResetControlContainer;
