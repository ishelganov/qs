import { SET_SELECTED_CACHE_NODE, SET_SELECTED_DB_NODE } from '../constants';

/**
 * Select cache node
 * @param id
 */
export const selectCacheNode = (id: string) => ({
    type: SET_SELECTED_CACHE_NODE,
    payload: {
        data: {
            id,
        },
    },
});

/**
 * Select database node
 * @param id
 */
export const selectDbNode = (id: string) => ({
    type: SET_SELECTED_DB_NODE,
    payload: {
        data: {
            id,
        },
    },
});