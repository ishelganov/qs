import {SET_SELECTED_CACHE_NODE, SET_SELECTED_DB_NODE} from '../../constants';
import {selectCacheNode, selectDbNode} from '../selectNode';

describe('ui actions', function () {
    it('test selection cache node', () => {
        const expectedAction = {
            type: SET_SELECTED_CACHE_NODE,
            payload: {
                data: {
                    id: '776',
                },
            },
        };

        expect(selectCacheNode('776')).toEqual(expectedAction);
    });

    it('test selection db node', () => {
        const expectedAction = {
            type: SET_SELECTED_DB_NODE,
            payload: {
                data: {
                    id: '1414',
                },
            },
        };

        expect(selectDbNode('1414')).toEqual(expectedAction);
    });
});
