import { ADD_NOTIFICATION } from '../constants';

const addNotification = (text: string) => ({
    type: ADD_NOTIFICATION,
    payload: {
        data: {
            text,
        },
    },
});

export default addNotification;
