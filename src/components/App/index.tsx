import * as React from 'react';
import './style.scss';
import '../../global.scss';

import Paper from '../Paper';
import DBTreeViewContainer from '../../containers/DBTreeView';
import CacheTreeViewContainer from '../../containers/CacheTreeView';
import Notification from '../Notification';

type Props = {};

const App: React.FC<Props> = () => {
    return (
        <>
            <div className={'container'}>
                <Paper className={'App'}>
                    <CacheTreeViewContainer />
                    <DBTreeViewContainer />
                </Paper>
            </div>
            <Notification />
        </>
    );
};

export default App;
