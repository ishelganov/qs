import { SYNC_CACHE } from '../constants';

type Options = {
    removedIds: string[];
};

const syncCache = (options: Options) => ({
    type: SYNC_CACHE,
    payload: {
        data: options,
    },
});

export default syncCache;
