import { AnyAction, applyMiddleware, createStore } from 'redux';
import thunkMiddleware, { ThunkAction } from 'redux-thunk';
import reducer, { rootState } from '../reducers';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

export type AppThunk<ReturnType = void, Actions = AnyAction> = ThunkAction<
    ReturnType,
    rootState,
    unknown,
    // @ts-ignore
    Actions
>;

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunkMiddleware)));

export default store;
