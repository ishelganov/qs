import * as React from 'react';
import cn from 'classnames';

import './style.scss';

type Props = {
    className?: string;
    disabled?: boolean;
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

const Button: React.FC<Props> = ({ className, children, disabled = false, onClick }) => {
    return (
        <button className={cn('Button', className)} disabled={disabled} onClick={onClick}>
            {children}
        </button>
    );
};

export default Button;
