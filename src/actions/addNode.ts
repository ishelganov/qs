import { ADD_NODE } from '../constants';

import { SingleNode } from '../components/Node';

type Options = {
    parentId: string;
} & SingleNode;

/**
 * Add new node
 * @param options
 */
export const addNode = (options: Options) => ({
    type: ADD_NODE,
    payload: {
        data: options,
    },
});

export default addNode;
