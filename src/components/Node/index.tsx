import * as React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import cn from 'classnames';

import { selectCacheNode, selectDbNode } from '../../actions/selectNode';

import { rootState } from '../../reducers';

import Tree from '../Tree';

import './style.scss';

export type SingleNode = {
    id: string;
    value: string;
    childIds: string[];
    removed: boolean;
};

type Props = {
    nodeId: string;
    type: 'db' | 'cache';
};

const Node: React.FC<Props> = ({ nodeId, type }) => {
    const [children, setChildren] = React.useState<string[]>([]);
    const items = useSelector((state: rootState) => state[type].items, shallowEqual);
    const selectedNode = useSelector((state: rootState) =>
        type === 'db' ? state.ui.activeDbNode : state.ui.activeCacheNode,
    );
    const dispatch = useDispatch();

    /**
     * Select clicked node and pass to ui
     */
    const setSelected = React.useCallback(
        (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            const { id } = e.target as HTMLDivElement;

            if (!items[nodeId].removed && selectedNode !== id) {
                type === 'db' ? dispatch(selectDbNode(id)) : dispatch(selectCacheNode(id));
            }
        },
        [dispatch, selectedNode, items, nodeId, type],
    );

    /**
     * Watch for node children
     */
    React.useEffect(() => {
        const ids: string[] = [];
        items &&
            items[nodeId] &&
            items[nodeId].childIds.forEach((childId) => {
                if (items.hasOwnProperty(childId)) {
                    ids.push(childId);
                }
            });
        setChildren(ids);
    }, [items, nodeId]);

    if (!items.hasOwnProperty(nodeId)) return null;

    return (
        <li>
            <div
                id={nodeId}
                className={cn(
                    'Node',
                    items[nodeId].removed && 'Node_Removed',
                    !items[nodeId].removed && selectedNode === nodeId && 'Node_Selected',
                )}
                onClick={setSelected}
            >
                {items[nodeId].value}
            </div>
            <Tree nodes={children} type={type} />
        </li>
    );
};

export default React.memo(Node);
