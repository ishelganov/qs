import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

import updateDbTree from '../../actions/updateDbTree';

import { rootState } from '../../reducers';

import Button from '../../components/Button';
import resetOperations from '../../actions/resetOperations';

type Props = {};

const ApplyControl: React.FC<Props> = () => {
    const dispatch: ThunkDispatch<rootState, unknown, AnyAction> = useDispatch();
    const operations = useSelector((state: rootState) => state.cache.operations);
    const [isFetching, setFetching] = React.useState(false);

    const isBtnDisabled = React.useMemo(() => {
        return (
            !(operations.add.length + operations.remove.length + operations.change.length) ||
            isFetching
        );
    }, [operations, isFetching]);

    const applyOnClick = () => {
        setFetching(true);
        dispatch(updateDbTree(operations)).then(() => {
            dispatch(resetOperations());
            setFetching(false);
        });
    };

    return (
        <Button disabled={isBtnDisabled} onClick={applyOnClick}>
            Apply
        </Button>
    );
};

export default ApplyControl;
