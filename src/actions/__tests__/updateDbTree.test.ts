import configureStore from 'redux-mock-store';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { RESET_CACHE, UPDATE_DB_TREE_SUCCESS, UPDATE_DB_TREE_REQUEST } from '../../constants';
import { rootState } from 'src/reducers';
import updateDbTree from '../updateDbTree';

type DispatchExts = ThunkDispatch<rootState, undefined, AnyAction>;

const middlewares = [thunk];
const mockStore = configureStore<Record<string, unknown>, DispatchExts>(middlewares);

describe('test update database tree action', () => {
    it('add new node', (done) => {
        const store = mockStore();
        const operations = {
            add: [
                {
                    parentId: '1',
                    id: '2',
                    value: '777',
                    childIds: [],
                    removed: false,
                },
            ],
            remove: [],
            change: [],
        };

        const expectedActions = [
            {
                type: UPDATE_DB_TREE_REQUEST,
            },
            {
                type: UPDATE_DB_TREE_SUCCESS,
                payload: {
                    data: operations,
                },
            },
            {
                type: RESET_CACHE,
            },
        ];
        store.dispatch(updateDbTree(operations)).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
            done();
        });
    });

    it('mark as removed node', (done) => {
        const store = mockStore();
        const operations = {
            add: [],
            remove: [
                {
                    id: '1',
                    removed: true,
                },
            ],
            change: [],
        };

        const expectedActions = [
            {
                type: UPDATE_DB_TREE_REQUEST,
            },
            {
                type: UPDATE_DB_TREE_SUCCESS,
                payload: {
                    data: operations,
                },
            },
            {
                type: RESET_CACHE,
            },
        ];
        store.dispatch(updateDbTree(operations)).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
            done();
        });
    });

    it('change node', (done) => {
        const store = mockStore();
        const operations = {
            add: [],
            remove: [],
            change: [
                {
                    id: '53',
                    value: '91',
                    removed: false,
                },
            ],
        };

        const expectedActions = [
            {
                type: UPDATE_DB_TREE_REQUEST,
            },
            {
                type: UPDATE_DB_TREE_SUCCESS,
                payload: {
                    data: operations,
                },
            },
            {
                type: RESET_CACHE,
            },
        ];
        store.dispatch(updateDbTree(operations)).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
            done();
        });
    });
});
