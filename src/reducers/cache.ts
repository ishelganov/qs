import {
    ADD_NODE,
    CHANGE_NODE,
    GET_NODE_SUCCESS,
    REMOVE_NODE,
    RESET_APP,
    UPDATE_CACHE_TREE,
    UPDATE_OPERATIONS,
    GET_NODE_REQUEST,
    SYNC_CACHE,
    RESET_OPERATIONS,
    UPDATE_CACHE_ITEMS,
} from '../constants';

import { getNodeRequest, getNodeSuccess } from '../actions/getNode';
import { updateCacheTreeSuccess } from '../actions/updateCacheTree';
import { AddOperations, ChangeOperations, RemoveOperations } from '../actions/updateDbTree';
import addNode from '../actions/addNode';
import removeNode from '../actions/removeNode';
import changeNode from '../actions/changeNode';
import resetApp from '../actions/resetApp';
import updateOperations from '../actions/updateOperations';
import syncCache from '../actions/syncCache';
import resetOperations from '../actions/resetOperations';

import { SingleNode } from '../components/Node';
import { markRemove } from 'src/utils';
import { updateCacheItemsSuccess } from '../actions/updateCacheItems';

type CacheState = {
    isFetching: boolean;
    items: {
        [key: string]: SingleNode;
    };
    tree: string[];
    operations: {
        add: AddOperations;
        remove: RemoveOperations;
        change: ChangeOperations;
    };
};

const defaulState = {
    isFetching: false,
    items: {},
    tree: [],
    operations: {
        add: [],
        remove: [],
        change: [],
    },
};

type Actions =
    | ReturnType<typeof getNodeRequest>
    | ReturnType<typeof getNodeSuccess>
    | ReturnType<typeof updateCacheTreeSuccess>
    | ReturnType<typeof addNode>
    | ReturnType<typeof removeNode>
    | ReturnType<typeof updateOperations>
    | ReturnType<typeof changeNode>
    | ReturnType<typeof resetApp>
    | ReturnType<typeof resetOperations>
    | ReturnType<typeof syncCache>
    | ReturnType<typeof updateCacheItemsSuccess>;

const cache = (state: CacheState = defaulState, action: Actions) => {
    switch (action.type) {
        case GET_NODE_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }
        case GET_NODE_SUCCESS: {
            const { id, value, childIds, removed } = action.payload.data.item;
            const { shouldCreateTree } = action.payload.data;

            return {
                ...state,
                isFetching: false,
                items: {
                    ...state.items,
                    [id]: {
                        id,
                        value,
                        childIds,
                        removed,
                    },
                },
                tree: shouldCreateTree ? [...state.tree, id] : [...state.tree],
            };
        }

        case UPDATE_CACHE_TREE: {
            const { treeIds } = action.payload.data;
            return {
                ...state,
                items: {
                    ...state.items,
                },
                tree: [...treeIds],
            };
        }

        case UPDATE_CACHE_ITEMS: {
            const { removedIds } = action.payload.data;

            const newItems: { [key: string]: SingleNode } = {};

            removedIds.forEach((id) => {
                newItems[id] = {
                    ...state.items[id],
                    removed: true,
                };
            });
            return {
                ...state,
                items: {
                    ...state.items,
                    ...newItems,
                },
            };
        }

        case ADD_NODE: {
            const { id, value, childIds, parentId, removed } = action.payload.data;

            return {
                ...state,
                items: {
                    ...state.items,
                    [parentId]: {
                        ...state.items[parentId],
                        childIds: [...state.items[parentId].childIds, id],
                    },
                    [id]: {
                        id,
                        value,
                        childIds,
                        removed,
                    },
                },
            };
        }

        case REMOVE_NODE: {
            const { id } = action.payload.data;

            return {
                ...state,
                items: {
                    ...state.items,
                    [id]: {
                        ...state.items[id],
                        removed: true,
                    },
                },
            };
        }

        case CHANGE_NODE: {
            const { id, value } = action.payload.data;

            return {
                ...state,
                items: {
                    ...state.items,
                    [id]: {
                        ...state.items[id],
                        value,
                    },
                },
            };
        }

        case UPDATE_OPERATIONS: {
            const { operation, item } = action.payload.data;

            return {
                ...state,
                operations: {
                    ...state.operations,
                    [operation]: [...state.operations[operation], item],
                },
            };
        }

        case SYNC_CACHE: {
            const { removedIds } = action.payload.data;
            const removedItems: { [key: string]: SingleNode } = {};

            removedIds.forEach((removeId) => {
                if (state.items.hasOwnProperty(removeId)) {
                    const children = markRemove(state.items, state.items[removeId]);

                    children.forEach((childId) => {
                        removedItems[childId] = {
                            ...state.items[childId],
                            removed: true,
                        };
                    });
                }
            });

            return {
                ...state,
                items: {
                    ...state.items,
                    ...removedItems,
                },
            };
        }

        case RESET_OPERATIONS: {
            return {
                ...state,
                operations: {
                    add: [],
                    remove: [],
                    change: [],
                },
            };
        }

        case RESET_APP: {
            return defaulState;
        }

        default: {
            return state;
        }
    }
};

export default cache;
