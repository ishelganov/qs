import { RESET_APP, UPDATE_DB_TREE_SUCCESS, UPDATE_DB_TREE_REQUEST } from '../constants';

import { updateDbTreeRequest, updateDbTreeSuccess } from '../actions/updateDbTree';
import resetApp from '../actions/resetApp';

import { SingleNode } from '../components/Node';

type DbState = {
    isFetching: boolean;
    items: {
        [key: string]: SingleNode;
    };
    tree: string[];
};

const defaulState = {
    isFetching: false,
    items: {
        1: {
            id: '1',
            value: '3',
            childIds: ['2', '6'],
            removed: false,
        },
        2: {
            id: '2',
            value: '5',
            childIds: ['3', '4'],
            removed: false,
        },
        3: {
            id: '3',
            value: '10',
            childIds: [],
            removed: false,
        },
        4: {
            id: '4',
            value: '11',
            childIds: ['5'],
            removed: false,
        },
        5: {
            id: '5',
            value: '9',
            childIds: ['9'],
            removed: false,
        },
        6: {
            id: '6',
            value: '7',
            childIds: ['7', '8'],
            removed: false,
        },
        7: {
            id: '7',
            value: '10',
            childIds: [],
            removed: false,
        },
        8: {
            id: '8',
            value: '33',
            childIds: [],
            removed: false,
        },
        9: {
            id: '9',
            value: '17',
            childIds: [],
            removed: false,
        },
    },
    tree: ['1'],
};

type Actions =
    | ReturnType<typeof updateDbTreeRequest>
    | ReturnType<typeof updateDbTreeSuccess>
    | ReturnType<typeof resetApp>;
const db = (state: DbState = defaulState, action: Actions) => {
    switch (action.type) {
        case UPDATE_DB_TREE_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }
        case UPDATE_DB_TREE_SUCCESS: {
            const { updatedItems } = action.payload.data;

            return {
                ...state,
                isFetching: false,
                items: {
                    ...updatedItems,
                },
            };
        }

        case RESET_APP: {
            return defaulState;
        }
        default: {
            return state;
        }
    }
};

export default db;
