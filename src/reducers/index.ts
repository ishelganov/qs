import { combineReducers } from 'redux';
import cache from './cache';
import ui from './ui';
import db from './db';

const reducer = combineReducers({
    cache,
    db,
    ui,
});

export type rootState = ReturnType<typeof reducer>;

export default reducer;
