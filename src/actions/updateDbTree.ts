import { UPDATE_DB_TREE_SUCCESS, UPDATE_DB_TREE_REQUEST } from '../constants';
import { AppThunk } from '../store';

import { markRemove } from '../utils';
import syncCache from './syncCache';
import { SingleNode } from '../components/Node';

export type AddOperations = {
    parentId: string;
    id: string;
    value: string;
    childIds: string[];
    removed: boolean;
}[];

export type RemoveOperations = {
    id: string;
    removed: boolean;
}[];

export type ChangeOperations = {
    id: string;
    value: string;
    removed: boolean;
}[];

type Options = {
    add: AddOperations;
    remove: RemoveOperations;
    change: ChangeOperations;
};

export const updateDbTreeRequest = () => ({
    type: UPDATE_DB_TREE_REQUEST,
});

type UpdateDbTreeSuccessOptions = {
    updatedItems: { [key: string]: SingleNode };
};
export const updateDbTreeSuccess = (options: UpdateDbTreeSuccessOptions) => ({
    type: UPDATE_DB_TREE_SUCCESS,
    payload: {
        data: options,
    },
});

export type Actions =
    | ReturnType<typeof updateDbTreeRequest>
    | ReturnType<typeof updateDbTreeSuccess>
    | ReturnType<typeof syncCache>;

/**
 * Update database tree after applying all changes
 * @param options
 */
export const updateDbTree = (options: Options): AppThunk<Promise<void>, Actions> => {
    // these calculations should be on the backend so we need just throw all changes to backend

    return (dispatch, getState) => {
        dispatch(updateDbTreeRequest());

        return new Promise((resolve) => {
            const items = getState().db.items;
            //imitate async
            setTimeout(() => {
                // REMOVING OPERATIONS
                const idsForRemove: string[] = [];
                const removedItems: { [key: string]: SingleNode } = {};
                options.remove.forEach((operation) => {
                    const { id } = operation;

                    if (items.hasOwnProperty(id) && !items[id].removed)
                        idsForRemove.push(...markRemove(items, items[id]));
                });

                idsForRemove.forEach((id) => {
                    removedItems[id] = {
                        ...items[id],
                        removed: true,
                    };
                });

                const updatedItems: { [key: string]: SingleNode } = {
                    ...items,
                    ...removedItems,
                };

                // ADDING OPERATIONS
                options.add.forEach((operation) => {
                    const { parentId, id, value, childIds, removed } = operation;
                    let _existInAdd = false;

                    options.remove.forEach((itemToRemove) => {
                        if (itemToRemove.id === id) _existInAdd = true;
                    });

                    if (
                        updatedItems.hasOwnProperty(parentId) &&
                        !updatedItems[parentId].removed &&
                        !_existInAdd
                    ) {
                        // add to parent children array
                        updatedItems[parentId].childIds = [...updatedItems[parentId].childIds, id];

                        // add new item
                        updatedItems[id] = {
                            id,
                            value,
                            childIds,
                            removed,
                        };
                    }
                });

                // CHANGES OPERATIONS
                options.change.forEach((operation) => {
                    const { id, value } = operation;

                    if (updatedItems.hasOwnProperty(id) && !updatedItems[id].removed) {
                        updatedItems[id] = {
                            ...updatedItems[id],
                            value,
                        };
                    }
                });

                dispatch(updateDbTreeSuccess({ updatedItems }));
                dispatch(syncCache({ removedIds: idsForRemove }));
                resolve();
            }, 1000);
        });
    };
};

export default updateDbTree;
