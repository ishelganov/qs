import { UPDATE_CACHE_ITEMS } from '../constants';
import { AppThunk } from '../store';
import { setRemoved } from '../utils';

type Options = {
    removedIds: string[];
};

export const updateCacheItemsSuccess = (options: Options) => ({
    type: UPDATE_CACHE_ITEMS,
    payload: {
        data: options,
    },
});

/**
 * Find removed items and mark as removed it's children
 */
const updateCacheItems = (): AppThunk => {
    return (dispatch, getState) => {
        const items = getState().cache.items;
        const removedIds: string[] = [];

        Object.values(items).forEach((item) => {
            if (item.removed) {
                const removedArr = setRemoved(items, item);
                removedArr.map((removedItemId) => {
                    if (!removedIds.includes(removedItemId)) removedIds.push(removedItemId);
                });
            }
        });

        console.log(`should mark as removed: `, removedIds);
        dispatch(updateCacheItemsSuccess({ removedIds: removedIds }));
    };
};

export default updateCacheItems;
