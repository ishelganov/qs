import { REMOVE_NOTIFICATION } from '../constants';

const removeNotification = () => ({
    type: REMOVE_NOTIFICATION,

});

export default removeNotification;
