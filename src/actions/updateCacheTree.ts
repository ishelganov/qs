import { AppThunk } from '../store';
import { UPDATE_CACHE_TREE } from '../constants';

type Options = {
    treeIds: string[];
};

export const updateCacheTreeSuccess = (options: Options) => ({
    type: UPDATE_CACHE_TREE,
    payload: {
        data: options,
    },
});

/**
 * Update cache tree if it found relationship, create right order
 */
const updateCacheTree = (): AppThunk => {
    return (dispatch, getState) => {
        const tree = getState().cache.tree;
        const items = getState().cache.items;
        // node ids that render in separate tree but found in children
        const ids: string[] = [];
        const newIds = [...tree];

        tree.forEach((node) => {
            Object.values(items).forEach((item) => {
                // see all root nodes and try find in items children, save in ids
                if (item.childIds.includes(node)) {
                    ids.push(node);
                }
            });
        });

        if (ids.length > 0) {
            for (let i = 0; i < ids.length; i++) {
                const pos = newIds.indexOf(ids[i]);
                if (pos !== -1) {
                    newIds.splice(pos, 1);
                }
            }

            dispatch(updateCacheTreeSuccess({ treeIds: newIds }));
        }
    };
};

export default updateCacheTree;
