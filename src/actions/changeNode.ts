import { CHANGE_NODE } from '../constants';

type Options = {
    id: string;
    value: string;
};

/**
 * Change node value
 * @param options
 */
const changeNode = (options: Options) => ({
    type: CHANGE_NODE,
    payload: {
        data: options,
    },
});

export default changeNode;
