import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import updateOperations from '../../actions/updateOperations';
import changeNode from '../../actions/changeNode';

import { rootState } from '../../reducers';

import Modal from '../../components/Modal';

type Props = {
    closePortal: () => void;
};

const ChangeNodeModalContainer: React.FC<Props> = ({ closePortal }) => {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState('');
    const selectedNode = useSelector((state: rootState) => state.ui.activeCacheNode);

    const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
    };

    const onClickSave = () => {
        if (selectedNode) {
            dispatch(
                changeNode({
                    id: selectedNode,
                    value,
                }),
            );
            dispatch(
                updateOperations({
                    operation: 'change',
                    item: {
                        id: selectedNode,
                        value: value,
                        removed: false,
                    },
                }),
            );
        }

        closePortal();
    };

    return (
        <Modal
            title={'Enter new value'}
            value={value}
            onClickSave={onClickSave}
            onChangeInput={onChangeInput}
        />
    );
};

export default ChangeNodeModalContainer;
