import { SingleNode } from './components/Node';

/**
 * Util function to recursive mark as removing
 * @param items
 * @param item
 */
export const markRemove = (items: { [key: string]: SingleNode }, item: SingleNode) => {
    const _ids: string[] = [item.id];

    function recurse(childsIds: string[]) {
        if (item.childIds.length === 0) _ids.push(item.id);
        else {
            childsIds.forEach((childId) => {
                if (items[childId] && items[childId].childIds.length > 0)
                    recurse(items[childId].childIds);
                if (items[childId] && !items[childId].removed) _ids.push(childId);
            });
        }
    }

    if (item.childIds.length > 0) recurse(item.childIds);

    return _ids;
};

/**
 * recursive mark as removing (V2)
 * @param items
 * @param item
 * @private
 */
export const setRemoved = (items: { [key: string]: SingleNode }, item: SingleNode) => {
    const _ids: string[] = [];

    function recurse(item: SingleNode) {
        if (item.childIds.length > 0) {
            item.childIds.forEach((childId) => {
                if (items[childId]) recurse(items[childId]);
            });
        }
        _ids.push(item.id);
    }
    recurse(item);

    return _ids;
};
