import { RESET_OPERATIONS } from '../constants';

const resetOperations = () => ({
    type: RESET_OPERATIONS,
});

export default resetOperations;
