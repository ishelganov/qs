import * as React from 'react';
import { Portal } from 'react-portal';
import { useSelector } from 'react-redux';
import { rootState } from '../../reducers';

import './style.scss';

type Props = {};

const Notification: React.FC<Props> = (props) => {
    const notification = useSelector((state: rootState) => state.ui.notification);

    return (
        <>
            {notification && (
                <Portal node={document && document.getElementById('NotificationList')}>
                    <div className={'Notification'}>{notification}</div>
                </Portal>
            )}
        </>
    );
};

export default Notification;
