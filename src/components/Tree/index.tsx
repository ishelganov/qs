import * as React from 'react';

import Node from '../Node/';

type Props = {
    nodes: string[];
    type: 'db' | 'cache';
};

const Tree: React.FC<Props> = ({ nodes, type }) => {
    return nodes && nodes.length > 0 ? (
        <ul>
            {nodes.map((node: string) => {
                return <Node key={node} nodeId={node} type={type} />;
            })}
        </ul>
    ) : null;
};

export default React.memo(Tree);
