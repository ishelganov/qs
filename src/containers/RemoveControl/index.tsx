import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import removeNode from '../../actions/removeNode';

import { rootState } from '../../reducers';

import Button from '../../components/Button';
import updateOperations from '../../actions/updateOperations';

import { markRemove } from '../../utils';

type Props = {
    disabled: boolean;
};

const RemoveControl: React.FC<Props> = ({ disabled }) => {
    const selectedNode = useSelector((state: rootState) => state.ui.activeCacheNode);
    const items = useSelector((state: rootState) => state.cache.items);
    const dispatch = useDispatch();

    /**
     * Handler for remove clicking
     */
    const onRemoveNodeClick = () => {
        if (selectedNode) {
            const idsForRemove = markRemove(items, items[selectedNode]);

            idsForRemove.forEach((id) => {
                dispatch(
                    removeNode({
                        id: id,
                    }),
                );
                dispatch(
                    updateOperations({
                        operation: 'remove',
                        item: {
                            id: id,
                            removed: true,
                        },
                    }),
                );
            });
        }
    };

    return (
        <Button disabled={disabled} onClick={onRemoveNodeClick}>
            -
        </Button>
    );
};

export default React.memo(RemoveControl);
