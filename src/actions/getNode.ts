import { GET_NODE_REQUEST, GET_NODE_SUCCESS } from '../constants';
import { AppThunk } from '../store';
import { SingleNode } from '../components/Node';

type GetNodeSuccessOptions = {
    item: SingleNode;
    shouldCreateTree: boolean;
};

export type Actions = ReturnType<typeof getNodeRequest> | ReturnType<typeof getNodeSuccess>;

export const getNodeRequest = () => ({
    type: GET_NODE_REQUEST,
});

export const getNodeSuccess = (data: GetNodeSuccessOptions) => ({
    type: GET_NODE_SUCCESS,
    payload: {
        data: data,
    },
});

/**
 * Imitate api method to get node
 * @param node
 */
export const getNode = (node: SingleNode): AppThunk<Promise<void>, Actions> => {
    return (dispatch, getState) => {
        //imitate async action
        return new Promise((resolve, reject) => {
            const { id, value, childIds, removed } = node;
            const items = getState().cache.items;
            const cacheTree = getState().cache.tree;

            if (items.hasOwnProperty(id)) {
                reject();
            } else {
                let shouldCreateTree = true;

                if (cacheTree.includes(id)) shouldCreateTree = false;

                if (shouldCreateTree) {
                    Object.values(items).forEach((item) => {
                        item.childIds.forEach((childId: string) => {
                            if (childId === id) shouldCreateTree = false;
                        });
                    });
                }

                dispatch(getNodeRequest());

                setTimeout(() => {
                    dispatch(
                        getNodeSuccess({
                            item: {
                                id,
                                value,
                                childIds,
                                removed: removed,
                            },
                            shouldCreateTree,
                        }),
                    );
                    resolve();
                }, 100);
            }
        });
    };
};
