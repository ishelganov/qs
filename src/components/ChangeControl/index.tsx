import * as React from 'react';
import { PortalWithState } from 'react-portal';

import Button from '../Button';
import ChangeNodeModalContainer from '../../containers/ChangeNodeModal';

type Props = {
    disabled: boolean;
};

const ChangeControl: React.FC<Props> = ({ disabled }) => {
    return (
        <PortalWithState closeOnEsc={true} closeOnOutsideClick={true}>
            {({ openPortal, closePortal, portal }) => (
                <>
                    <Button disabled={disabled} onClick={openPortal}>
                        Change
                    </Button>
                    {portal(<ChangeNodeModalContainer closePortal={closePortal} />)}
                </>
            )}
        </PortalWithState>
    );
};

export default React.memo(ChangeControl);
